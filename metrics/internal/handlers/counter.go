package handlers

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/gitlab-org/release-tools/metrics/internal/metrics"
)

type counter struct {
	metric metrics.Counter
}

func NewCounter(metric metrics.Counter) Pluggable {
	return &counter{metric}
}

func (c *counter) PlugRoutes(r *mux.Router) {
	subRouter := r.PathPrefix(route(c.metric)).Subrouter()

	subRouter.Methods("DELETE").HandlerFunc(c.resetHandlerFunc)
	subRouter.HandleFunc("/inc", c.incHandlerFunc)
	subRouter.HandleFunc("/add", c.addHandlerFunc)
}

func (c *counter) incHandlerFunc(w http.ResponseWriter, r *http.Request) {
	labels := getLabels(r)
	if err := c.metric.CheckLabels(labels); err != nil {
		badRequest(w, r, err.Error())

		return
	}

	c.metric.Inc(labels...)

	answer(w, r, "Incremented")
}

func (c *counter) addHandlerFunc(w http.ResponseWriter, r *http.Request) {
	value, err := getValue(r)
	if err != nil {
		badRequest(w, r, err.Error())

		return
	}

	labels := getLabels(r)
	if err := c.metric.CheckLabels(labels); err != nil {
		badRequest(w, r, err.Error())

		return
	}

	c.metric.Add(value, labels...)

	answer(w, r, "Added")
}

func (c *counter) resetHandlerFunc(w http.ResponseWriter, r *http.Request) {
	c.metric.Reset()

	answer(w, r, "Reset")
}
