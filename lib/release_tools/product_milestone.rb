# frozen_string_literal: true

module ReleaseTools
  # ProductMilestone fetches current or upcoming milestones for the GitLab
  # product based on version.
  class ProductMilestone
    def self.current
      version = ReleaseTools::ReleaseManagers::Schedule.new.active_version

      milestone_by_title(version.milestone_name)
    end

    def self.after(version)
      upcoming_minor = ReleaseTools::Version.new(version.next_minor)
      upcoming_major = ReleaseTools::Version.new(version.next_major)

      # If we can't find the milestone by searching for the next minor
      # version, try the next major one.
      milestone_by_title(upcoming_minor.milestone_name) ||
        milestone_by_title(upcoming_major.milestone_name)
    end

    def self.next
      current = ReleaseTools::ReleaseManagers::Schedule.new.active_version
      after(current)
    end

    def self.milestone_by_title(title)
      Retriable.with_context(:api) do
        ReleaseTools::GitlabClient.group_milestones(
          Project::GitlabEe.group,
          state: 'active',
          title: title
        ).first
      end
    end
  end
end
