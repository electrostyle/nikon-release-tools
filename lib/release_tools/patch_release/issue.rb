# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    class Issue < ReleaseTools::Issue
      BLOG_MR_STRING_IN_DESCRIPTION = 'BLOG_POST_MR'

      def title
        if version.nil?
          "Releases #{versions_title}"
        else
          "Release #{version.to_ce}"
        end
      end

      def labels
        'Monthly Release'
      end

      def project
        ReleaseTools::Project::Release::Tasks
      end

      def monthly_issue
        @monthly_issue ||= ReleaseTools::MonthlyIssue.new(version: version)
      end

      def link!
        return if version.nil? || version.monthly?

        ReleaseTools::GitlabClient.link_issues(self, monthly_issue)
      end

      def assignees
        ReleaseManagers::Schedule.new.active_release_managers.collect(&:id)
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        nil
      end

      def blog_post_merge_request
        @blog_post_merge_request ||= if version.nil?
                                       ReleaseTools::PatchRelease::BlogMergeRequest.new(
                                         project: ReleaseTools::Project::WWWGitlabCom,
                                         content: patch_release_coordinator.merge_requests(with_patch_version: true)
                                       )
                                     else
                                       ReleaseTools::PatchRelease::BlogMergeRequest.new(
                                         project: ReleaseTools::Project::WWWGitlabCom,
                                         version: version
                                       )
                                     end
      end

      def add_blog_mr_to_description(blog_mr_url)
        return if dry_run?

        Retriable.with_context(:api) do
          current_description = remote_issuable.description

          GitlabClient.edit_issue(
            project.path,
            iid,
            description: current_description.sub(BLOG_MR_STRING_IN_DESCRIPTION, blog_mr_url)
          )
        end
      end

      def projects
        ReleaseTools::ManagedVersioning::PROJECTS - [ReleaseTools::Project::GitlabEe]
      end

      # Using the first version until the maintenance policy is
      # officially extended
      def versions
        [patch_release_coordinator.versions.first]
      end

      def versions_title
        versions.join(', ')
      end

      def current_version
        versions.first.to_minor
      end

      def omnibus_package(ee: true)
        versions.first.to_omnibus(ee: ee)
      end

      def omnibus_package_for_the_current_version
        versions.first.to_omnibus(ee: true).tr('+', '-')
      end

      protected

      def template_path
        File.expand_path('../../../templates/patch_multi_version.md.erb', __dir__)
      end

      def patch_release_coordinator
        ReleaseTools::PatchRelease::Coordinator.new
      end
    end
  end
end
