# frozen_string_literal: true

module ReleaseTools
  module Qa
    class UsernameExtractor
      COMMUNITY_CONTRIBUTION_LABEL = "Community contribution"

      attr_reader :merge_request

      def initialize(merge_request)
        @merge_request = merge_request
      end

      def extract_usernames
        mentions_for_mr
          .map { |mention| username_format(mention) }
          .join(' ')
      end

      private

      def mentions_for_mr
        if merge_request.labels.include?(COMMUNITY_CONTRIBUTION_LABEL) && merge_request.merged_by&.username
          return [merge_request.merged_by&.username]
        end

        assignees = merge_request.assignees.collect(&:username)
        return assignees unless assignees.empty?

        [merge_request.author.username]
      end

      def username_format(username)
        "@#{username}"
      end
    end
  end
end
