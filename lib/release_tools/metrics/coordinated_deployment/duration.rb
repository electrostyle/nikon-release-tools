# frozen_string_literal: true

module ReleaseTools
  module Metrics
    module CoordinatedDeployment
      class Duration
        include ::SemanticLogger::Loggable

        # deploy_version: Omnibus package to be deployed
        # start_time: Start time as a string of the deployment
        def initialize(deploy_version:, start_time:)
          @client = ReleaseTools::Metrics::Client.new
          @deploy_version = deploy_version
          @start_time = Time.parse(start_time)
          @end_time = Time.now.utc
        end

        def execute
          logger.info('Recording duration', deploy_version: @deploy_version, start_time: @start_time, end_time: @end_time, duration: duration_in_seconds)

          record_histogram_metric
          record_gauge_metric
        end

        private

        def record_histogram_metric
          @client.observe(
            'deployment_duration_seconds',
            duration_in_seconds,
            labels: 'coordinator_pipeline,success'
          )
        end

        def record_gauge_metric
          @client.set(
            'deployment_duration_last_seconds',
            duration_in_seconds,
            labels: "coordinator_pipeline,success,#{@deploy_version}"
          )
        end

        def duration_in_seconds
          @end_time - @start_time
        end
      end
    end
  end
end
