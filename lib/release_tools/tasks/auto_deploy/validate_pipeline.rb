# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      class ValidatePipeline
        include ::SemanticLogger::Loggable

        GITLAB_PROJECT = Project::GitlabEe

        def execute
          version = ProductVersion.last_auto_deploy
          omnibus = version.auto_deploy_package
          log_payload = { product_version: version.version }

          logger.info('current version', log_payload.merge(omnibus: omnibus))

          deploy_version = ENV.fetch('DEPLOY_VERSION', nil)
          if deploy_version != omnibus
            logger.fatal('version mismatch', log_payload.merge(deploy_version: deploy_version, omnibus: omnibus))
          end

          gitlab_sha = version[GITLAB_PROJECT.metadata_project_name]&.sha
          unless gitlab_sha
            logger.error('cannot find the package gitlab SHA', log_payload)

            return
          end

          commits = Commits.new(GITLAB_PROJECT)
          success = commits.success_for_auto_deploy?(gitlab_sha)
          label = if success
                    logger.info('successful pipeline', log_payload.merge(gitlab_sha: gitlab_sha))

                    'success'
                  else
                    logger.error('unsuccessful pipeline', log_payload.merge(gitlab_sha: gitlab_sha))

                    'failed'
                  end

          ReleaseTools::Metrics::Client.new.inc("auto_deploy_gitlab_pipeline_total", labels: label)

          exit(1) unless success
        end
      end
    end
  end
end
