# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      class PlayManualJobs
        include ::SemanticLogger::Loggable

        MAX_PREVIOUS_PIPELINES = 10

        MANUAL_JOBS = %w[metrics:trace-pipeline].freeze

        def initialize(current_pipeline_id)
          @current_pipeline_id = current_pipeline_id
        end

        def execute
          # Play the manual jobs of the current pipeline
          play_manual_jobs(current_pipeline_id)

          # Play the manual jobs of upto 10 previous pipelines
          previous_stopped_pipelines.each do |pipeline|
            play_manual_jobs(pipeline.id)
          end
        end

        private

        attr_reader :current_pipeline_id

        def play_manual_jobs(pipeline_id)
          jobs = manual_jobs(pipeline_id)
          return if jobs.empty?

          jobs.each { |job| play_job(job) }
        end

        def manual_jobs(pipeline_id)
          jobs = ReleaseTools::GitlabOpsClient
                   .pipeline_jobs(ReleaseTools::Project::ReleaseTools, pipeline_id, { per_page: 70 })
                   .auto_paginate

          jobs.select { |j| MANUAL_JOBS.include?(j.name) }
        end

        def play_job(job)
          unless job.status == 'manual'
            logger.info('Not running job since it is not in manual state', job_name: job.name, job_url: job.web_url, status: job.status)
            return
          end

          logger.info("Running manual job", job_name: job.name, job_url: job.web_url, dry_run: SharedStatus.dry_run?)
          return if SharedStatus.dry_run?

          ReleaseTools::GitlabOpsClient.job_play(ReleaseTools::Project::ReleaseTools, job.id)
        end

        def previous_stopped_pipelines
          # We only want the first page of results.
          pipelines =
            ReleaseTools::GitlabOpsClient.pipelines(
              ReleaseTools::Project::ReleaseTools,
              {
                scope: 'tags',
                # Get 1.5 times the number of pipelines because there might be newer pipelines we need to sift through
                # before we get to the pipelines that ran before current_pipeline_id.
                # Also, one of the pipelines returned by the API will be the current_pipeline_id.
                per_page: (1.5 * MAX_PREVIOUS_PIPELINES).to_i
              }
            )

          pipelines
            .select { |p| pipeline_older_than_current?(p) && pipeline_stopped?(p) }
            .take(MAX_PREVIOUS_PIPELINES)
        end

        def pipeline_older_than_current?(pipeline)
          pipeline.id < current_pipeline_id
        end

        def pipeline_stopped?(pipeline)
          %w[success failed canceled manual].include?(pipeline.status)
        end
      end
    end
  end
end
