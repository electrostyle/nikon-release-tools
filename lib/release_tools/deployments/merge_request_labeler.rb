# frozen_string_literal: true

module ReleaseTools
  module Deployments
    # Adding of environment labels to deployed merge requests.
    class MergeRequestLabeler
      include ::SemanticLogger::Loggable
      include ReleaseTools::Tracker::MergeRequestLabeler

      private

      def deployment_labels
        {
          'gstg-ref' => 'workflow::staging-ref',
          'gstg-cny' => 'workflow::staging-canary',
          'gstg' => 'workflow::staging',
          'gprd-cny' => 'workflow::canary',
          'gprd' => 'workflow::production',
          'pre' => 'released::candidate',
          'release' => 'released::published'
        }
      end
    end
  end
end
