# frozen_string_literal: true

namespace :trace do
  desc 'Trace a pipeline, given the pipeline URL'
  task :pipeline_url, [:pipeline_url, :service_name, :version] do |_t, args|
    require 'opentelemetry/exporter/otlp'
    require 'opentelemetry/sdk'

    pipeline_url = args[:pipeline_url]
    service_name = args[:service_name]
    version = args[:version]

    exporter_endpoint = ENV.fetch('GITLAB_O11Y_URL')
    exporter_namespace_token = ENV.fetch('GITLAB_O11Y_NAMESPACE_TOKEN')

    exporter = OpenTelemetry::Exporter::OTLP::Exporter.new(
      endpoint: exporter_endpoint,
      headers: { Authorization: "Bearer #{exporter_namespace_token}" }
    )
    batch_span_processor = OpenTelemetry::SDK::Trace::Export::BatchSpanProcessor.new(exporter)

    OpenTelemetry::SDK.configure do |c|
      c.service_name = service_name
      c.add_span_processor(batch_span_processor)
    end

    ReleaseTools::PipelineTracer::Service.from_pipeline_url(pipeline_url, pipeline_name: version, trace_depth: 3).execute

    status_code = batch_span_processor.force_flush

    case status_code
    when OpenTelemetry::SDK::Trace::Export::SUCCESS
      $stdout.puts 'Successfully flushed span processor'.colorize(:green)
    when OpenTelemetry::SDK::Trace::Export::FAILURE
      $stdout.puts 'Failed to flush span processor'.colorize(:red)
      abort
    when OpenTelemetry::SDK::Trace::Export::TIMEOUT
      $stdout.puts 'Timed out waiting to flush span processor'.colorize(:red)
      abort
    else
      $stdout.puts "Unknown return value from force_flush: #{status_code}".colorize(:red)
      abort
    end
  end
end
