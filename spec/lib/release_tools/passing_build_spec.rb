# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PassingBuild do
  let(:fake_commit) { double('Commit', id: SecureRandom.hex(20)) }
  let(:target_branch) { '11-10-auto-deploy-1234' }

  subject(:service) { described_class.new(target_branch) }

  describe '#execute' do
    let(:fake_commits) { spy }

    before do
      stub_const('ReleaseTools::Commits', fake_commits)
    end

    it 'raises an error without a dev commit' do
      expect(fake_commits).to receive(:latest_successful_on_build)
        .with({ since_last_auto_deploy: true })
        .and_return(nil)

      expect { service.execute }
        .to raise_error(/Unable to find a passing/)
    end

    it 'returns the latest successful commit on Build' do
      expect(fake_commits)
        .to receive(:latest_successful_on_build)
        .with({ since_last_auto_deploy: true })
        .and_return(fake_commit)

      expect(service.execute).to eq(fake_commit)
    end

    it 'returns the latest commit when auto_deploy_tag_latest is enabled' do
      enable_feature(:auto_deploy_tag_latest)

      expect(fake_commits).to receive(:latest)
        .and_return(fake_commit)

      expect(service.execute).to eq(fake_commit)
    end
  end

  describe '#next_commit' do
    let(:commits_spy) { instance_spy(ReleaseTools::Commits) }

    before do
      allow(ReleaseTools::Commits).to receive(:new).and_return(commits_spy)
    end

    it 'calls commits.next_commit' do
      expect(service).to receive(:passing_build_commit).and_return(double(id: 'commit1'))
      expect(commits_spy).to receive(:next_commit).with('commit1').and_return(double(id: 'commit2'))

      expect(service.next_commit.id).to eq('commit2')
    end

    it 'raises error when passing_build_commit does not exist' do
      expect(commits_spy).to receive(:latest_successful_on_build)
        .with({ since_last_auto_deploy: true })
        .and_return(nil)

      expect { service.next_commit }.to raise_error(/Unable to find a passing/)
    end
  end
end
