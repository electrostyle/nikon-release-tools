# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::BlockerIssueFetcher do
  let(:release_issues) do
    [
      create(:issue, title: 'a', labels: ['release-blockers']),
      create(:issue, title: 'b', labels: ['release-blockers']),
      create(:issue, title: 'c', labels: ['release-blockers'])
    ]
  end

  let(:incidents) do
    [
      create(:issue, title: 'd', labels: ['Deploys-blocked-gstg::6hr']),
      create(:issue, title: 'e', labels: ['Deploys-blocked-gprd::10hr']),
      create(:issue, title: 'f', labels: ['Deploys-blocked-gprd::10hr']),
      create(:issue, title: 'g', labels: ['Service::Kube']),
      create(:issue, title: 'h', labels: ['Source::IMA::IncidentDeclare'])
    ]
  end

  subject(:blockers) { described_class.new }

  before do
    allow(blockers)
      .to receive(:release_blockers)
      .and_return(release_issues)

    allow(blockers)
      .to receive(:incidents)
      .and_return(incidents)
  end

  describe '#deployment_blockers' do
    it 'lists release and incidents with blocked-deploy labels' do
      blockers.fetch

      deployment_blockers = blockers.deployment_blockers

      expect(deployment_blockers.count).to eq(6)
      expect(deployment_blockers.map(&:title)).to eq(%w(a b c d e f))
    end
  end

  describe '#uncategorized_incidents' do
    it 'lists incidents with no deploys-blocked labels' do
      blockers.fetch

      incidents = blockers.uncategorized_incidents

      expect(incidents.count).to eq(2)
      expect(incidents.map(&:title)).to eq(%w(g h))
    end
  end
end
