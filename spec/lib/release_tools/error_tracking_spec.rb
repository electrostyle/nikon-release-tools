# frozen_string_literal: true

require 'spec_helper'
require 'sentry-raven'
require 'sentry-ruby'

describe ReleaseTools::ErrorTracking, :aggregate_failures do
  describe '.with_exception_captured' do
    subject { described_class.with_exception_captured { 1 / 0 } }

    it 'registers the exception with Raven and Sentry' do
      expect(Raven.instance).to receive(:capture_type)
        .with(an_instance_of(ZeroDivisionError), {})
        .and_call_original

      expect(Sentry).to receive(:capture_exception)
        .with(an_instance_of(ZeroDivisionError))
        .and_call_original

      expect { subject }.to raise_error ZeroDivisionError
    end
  end

  describe '.capture_exception' do
    let(:exception) { instance_double(StandardError) }

    it 'registers the exception with Raven and Sentry' do
      expect(Raven).to receive(:capture_exception).with(exception)
      expect(Sentry).to receive(:capture_exception).with(exception)

      described_class.capture_exception(exception)
    end
  end
end
